--
-- DESEMBARGADOR
--
WITH SIAP AS (
SELECT DECODE(DES.SERV_TIPO_MATRICULA, 0, 'T', 1, 'F', 2, 'M', 3, 'A', 5, 'E', 6, 'P', 'X') || LPAD(DES.SERV_MATRICULA, 7, '0') AS NUMERO_MATRICULA
       , SERV.NOME AS NOME
       , SERV.SEXO AS SEXO
       , SERV.DATA_NASCIMENTO AS DATA_NASCIMENTO
       , SERV.EMAIL AS EMAIL
       , SERV.CPF AS CPF
       , SERV.ESTADO_CIVIL AS ESTADO_CIVIL
       , SERV.USUARIO AS USUARIO
       , SERV.UF_SIGLA AS UF_SIGLA
       , SERV.MUNP_CODIGO AS MUNP_CODIGO
       , DES.IDENTIFICADOR_SITUACAO AS IDENTIFICADOR_SITUACAO
       , DES.DATA_DIST_ANT_APOSENTADORIA AS DATA_DIST_ANT_APOSENTADORIA
       , DES.INDICADOR_JUIZ_CONVOCADO AS INDICADOR_JUIZ_CONVOCADO
       , FDES.DESCRICAO AS DESCRICAO
    FROM SI_DESEMBARGADOR@SIAP_AN12 DES
    INNER JOIN SERVIDOR@SIAP_AN12 SERV ON
    (SERV.MATRICULA = DES.SERV_MATRICULA
     AND SERV.TIPO_MATRICULA = DES.SERV_TIPO_MATRICULA)
    LEFT JOIN FUNCAO_DESEMBARGADOR@SIAP_AN12 FDES ON
    (FDES.CODIGO = DES.FDES_CODIGO)
   WHERE DES.DATA_EXCLUSAO IS NULL
)
, RUPE AS (
  SELECT DISTINCT
         NUMERO_MATRICULA
       , NOME
       --, COD_INSTANCIA
    FROM (
            SELECT NVL(PFI.NUMERO_MATRICULA, 'NULO') AS NUMERO_MATRICULA
                 , PES.NOME
                 --, SER.COD_INSTANCIA
              FROM GRUPO GRU
                   JOIN USUARIO_GRUPO          UGR ON (GRU.ID           = UGR.GRUPO_ID)
                   JOIN USUARIO                USU ON (UGR.USUARIO_ID   = USU.ID)
                   JOIN PESSOA                 PES ON (USU.PESSOA_ID    = PES.ID)
                   JOIN PESSOA_FISICA          PFI ON (USU.PESSOA_ID    = PFI.ID)
             WHERE 1=1
               AND GRU.COD_TIPO = 0 -- Magistrado                   
/*                   LEFT JOIN USUARIO_SERVENTIA USEV ON (USU.ID           = USEV.USUARIO_ID)
                   LEFT JOIN SERVENTIA         SER ON (USEV.SERVENTIA_ID = SER.ID)
             WHERE 1=1
               AND GRU.COD_TIPO = 0 -- Magistrado
               AND SER.COD_INSTANCIA IN ( 0, 2 ) -- Confirmar se � s� a 2 ou a 3 tamb�m.*/
         )
  ORDER BY NUMERO_MATRICULA
)
SELECT T.*
     , COUNT(1) OVER (PARTITION BY NULL) AS TOTAL_COUNT
     , SUM(CASE WHEN EXISTE_NO_SIAP = 'Sim' AND EXISTE_NO_RUPE = 'Sim' THEN 1 ELSE 0 END) OVER (PARTITION BY NULL) AS AMBOS_COUNT
     , SUM(CASE WHEN EXISTE_NO_SIAP = 'Sim' AND EXISTE_NO_RUPE = 'N�o' THEN 1 ELSE 0 END) OVER (PARTITION BY NULL) AS SO_SIAP_COUNT
     , SUM(CASE WHEN EXISTE_NO_SIAP = 'N�o' AND EXISTE_NO_RUPE = 'Sim' THEN 1 ELSE 0 END) OVER (PARTITION BY NULL) AS SO_RUPE_COUNT
  FROM (
        SELECT CASE WHEN SIAP.NUMERO_MATRICULA IS NOT NULL THEN SIAP.NUMERO_MATRICULA  ELSE RUPE.NUMERO_MATRICULA END AS NUMERO_MATRICULA
             , CASE WHEN SIAP.NUMERO_MATRICULA IS NOT NULL THEN SIAP.NOME              ELSE RUPE.NOME  END AS NOME
             , CASE WHEN SIAP.NUMERO_MATRICULA IS NOT NULL THEN 'Sim' ELSE 'N�o' END AS EXISTE_NO_SIAP
             , CASE WHEN RUPE.NUMERO_MATRICULA IS NOT NULL THEN 'Sim' ELSE 'N�o' END AS EXISTE_NO_RUPE
             , CASE WHEN SIAP.NUMERO_MATRICULA IS NOT NULL AND RUPE.NUMERO_MATRICULA IS NULL THEN SYS.UTL_MATCH.EDIT_DISTANCE(SIAP.NOME, RUPE.NOME) END AS NOME_DIFF
             --, CASE WHEN SIAP.NUMERO_MATRICULA IS NOT NULL AND RUPE.NUMERO_MATRICULA IS NOT NULL THEN SYS.UTL_MATCH.EDIT_DISTANCE(SIAP.NOME, RUPE.NOME) END AS NOME_DIFF
          FROM SIAP
               FULL JOIN RUPE ON (SIAP.NUMERO_MATRICULA = RUPE.NUMERO_MATRICULA)
       ) T
ORDER BY EXISTE_NO_SIAP DESC
       , EXISTE_NO_RUPE DESC
       , NOME;
       
