drop table temp_mat;
create table temp_mat(matrupe varchar2(10) , matsiap varchar2(10));

declare 
  cursor ct is
  SELECT DISTINCT NUMERO_RUPE FROM ( SELECT NVL(PFI.NUMERO_MATRICULA, 'NULO') AS NUMERO_RUPE
                 , PES.NOME
              FROM GRUPO GRU
                   JOIN USUARIO_GRUPO          UGR ON (GRU.ID           = UGR.GRUPO_ID)
                   JOIN USUARIO                USU ON (UGR.USUARIO_ID   = USU.ID)
                   JOIN PESSOA                 PES ON (USU.PESSOA_ID    = PES.ID)
                   JOIN PESSOA_FISICA          PFI ON (USU.PESSOA_ID    = PFI.ID)
             WHERE 1=1
               AND GRU.COD_TIPO = 0 -- Magistrado                   
         );
         
  cursor dt is
  SELECT DECODE(DES.SERV_TIPO_MATRICULA, 0, 'T', 1, 'F', 2, 'M', 3, 'A', 5, 'E', 6, 'P', 'X') || LPAD(DES.SERV_MATRICULA, 7, '0') AS NUMERO_SIAP FROM SI_DESEMBARGADOR@SIAP_AN12 DES WHERE DES.DATA_EXCLUSAO IS NULL;
    
  mat_rupe varchar2(10);
  mat_siap varchar2(10);
  mat_min_siap varchar2(10);
  dist_mat NUMBER;
  dist_mat_temp NUMBER;
  var_sql varchar2(2048);
  
begin
 for rec in ct loop 
  mat_rupe := rec.NUMERO_RUPE;
  dist_mat := 10;
  for recd in dt loop
    mat_siap := recd.NUMERO_SIAP;
    dist_mat_temp := SYS.UTL_MATCH.EDIT_DISTANCE(mat_rupe, mat_siap);  
    if dist_mat_temp < dist_mat then
      dist_mat := dist_mat_temp;
      mat_min_siap := mat_siap;
    end if;
      
  end loop;
  if dist_mat < 3 then
    var_sql := 'insert into temp_mat (MATRUPE,MATSIAP) values(' || '''' || mat_rupe || '''' || ',' || '''' || mat_min_siap || '''' || ')';  
    execute IMMEDIATE var_sql;
  end if;
 end loop;
end;