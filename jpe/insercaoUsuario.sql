    insert 
    into
        ENDERECO
        (version, COD_TRIBUNAL, COMPLEMENTO, DATA_INCLUSAO, DSC_BAIRRO, DSC_ESTADO_PROVINCIA, DSC_LOGRADOURO, DSC_MUNICIPIO, E_MAIL, MUNICIPIO_ID, LOGRADOURO_BAIRRO_ID, NUMERO, NUMERO_CEP, PAIS_ID, TELEFONE_1, TELEFONE_2, COD_TIPO, TIPO_LOGRADOURO_ID, USUARIO_INCLUSAO_ID, id) 
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

    insert 
    into
        PESSOA
        (version, COD_TRIBUNAL, DATA_INCLUSAO, ENDERECO_CITACAO_ID, DESCRICAO_JUSTIF_DESCONHEC_DOC, NOME, NOME_FONETICO, COD_TIPO, COD_JUSTIF_DESCONHEC_DOCUMENTO, USUARIO_INCLUSAO_ID, usuario_ultima_atualizacao_id, id) 
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

    insert 
    into
        PESSOA_FISICA
        (COMPLEMENTO_OAB_MADEP, DATA_NASCIMENTO, DSC_MUNICIPIO_NATURALIDADE, DSC_OCUPACAO, COD_ESTADO_CIVIL, FALECIDO, MUNICIPIO_NATURALIDADE_ID, COD_NIVEL_ESCOLARIDADE, NOME_MAE, NOME_MAE_FONETICO, NOME_PAI, NOME_PAI_FONETICO, NUMERO_CNH, NUMERO_CPF, NUMERO_CTPS, NUMERO_IDENTIDADE, NUMERO_MATRICULA, NUMERO_OAB_MADEP, NUMERO_PASSAPORTE, NUMERO_PIS_PASEP, NUMERO_TITULO_ELEITOR, ORGAO_EXPEDIDOR_IDENTIDADE, PAIS_NACIONALIDADE_ID, PROCURADOR, SERIE_CTPS, SEXO, UF_CTPS_ID, UF_IDENTIDADE_ID, UF_OAB_MADEP_ID, id) 
    values
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)

    insert 
    into
        USUARIO
        (version, COD_SITUACAO_ATUAL, usuario_ultima_atualizacao_id, login, PESSOA_ID, senha, id) 
    values
        (?, ?, ?, ?, ?, ?, ?)


/*selecao para ??*/		
    select
        this_.id as id66_0_,
        this_.version as version66_0_,
        this_.nome as nome66_0_,
        this_.SERVENTIA_PADRAO_ID as SERVENTIA4_66_0_,
        this_.COD_TIPO as COD5_66_0_,
        this_.COD_TIPO_ACESSO as COD6_66_0_ 
    from
        GRUPO this_ 
    where
        (
            this_.nome like ? 
            and this_.COD_TIPO=?
        )
		
/*selecao para ??*/
    select
        this_.id as id166_0_,
        this_.version as version166_0_,
        this_.COD_SITUACAO_ATUAL as COD3_166_0_,
        this_.usuario_ultima_atualizacao_id as usuario4_166_0_,
        this_.GRUPO_ID as GRUPO5_166_0_,
        this_.USUARIO_ID as USUARIO6_166_0_ 
    from
        USUARIO_GRUPO this_ 
    where
        this_.GRUPO_ID=? 
        and this_.USUARIO_ID=?
		
/*selecao para ??*/		
    select
        this_.id as id109_3_,
        this_.version as version109_3_,
        this_.COD_SITUACAO_ATUAL as COD3_109_3_,
        this_.usuario_ultima_atualizacao_id as usuario6_109_3_,
        this_.login as login109_3_,
        this_.PESSOA_ID as PESSOA7_109_3_,
        this_.senha as senha109_3_,
        pessoa1_.id as id87_0_,
        pessoa1_.version as version87_0_,
        pessoa1_.COD_TRIBUNAL as COD3_87_0_,
        pessoa1_.DATA_INCLUSAO as DATA4_87_0_,
        pessoa1_.ENDERECO_CITACAO_ID as ENDERECO10_87_0_,
        pessoa1_.DESCRICAO_JUSTIF_DESCONHEC_DOC as DESCRICAO5_87_0_,
        pessoa1_.NOME as NOME87_0_,
        pessoa1_.NOME_FONETICO as NOME7_87_0_,
        pessoa1_.COD_TIPO as COD8_87_0_,
        pessoa1_.COD_JUSTIF_DESCONHEC_DOCUMENTO as COD9_87_0_,
        pessoa1_.USUARIO_INCLUSAO_ID as USUARIO11_87_0_,
        pessoa1_.usuario_ultima_atualizacao_id as usuario12_87_0_,
        pessoa1_1_.COMPLEMENTO_OAB_MADEP as COMPLEME1_88_0_,
        pessoa1_1_.DATA_NASCIMENTO as DATA2_88_0_,
        pessoa1_1_.DSC_MUNICIPIO_NATURALIDADE as DSC3_88_0_,
        pessoa1_1_.DSC_OCUPACAO as DSC4_88_0_,
        pessoa1_1_.COD_ESTADO_CIVIL as COD5_88_0_,
        pessoa1_1_.FALECIDO as FALECIDO88_0_,
        pessoa1_1_.MUNICIPIO_NATURALIDADE_ID as MUNICIPIO26_88_0_,
        pessoa1_1_.COD_NIVEL_ESCOLARIDADE as COD7_88_0_,
        pessoa1_1_.NOME_MAE as NOME8_88_0_,
        pessoa1_1_.NOME_MAE_FONETICO as NOME9_88_0_,
        pessoa1_1_.NOME_PAI as NOME10_88_0_,
        pessoa1_1_.NOME_PAI_FONETICO as NOME11_88_0_,
        pessoa1_1_.NUMERO_CNH as NUMERO12_88_0_,
        pessoa1_1_.NUMERO_CPF as NUMERO13_88_0_,
        pessoa1_1_.NUMERO_CTPS as NUMERO14_88_0_,
        pessoa1_1_.NUMERO_IDENTIDADE as NUMERO15_88_0_,
        pessoa1_1_.NUMERO_MATRICULA as NUMERO16_88_0_,
        pessoa1_1_.NUMERO_OAB_MADEP as NUMERO17_88_0_,
        pessoa1_1_.NUMERO_PASSAPORTE as NUMERO18_88_0_,
        pessoa1_1_.NUMERO_PIS_PASEP as NUMERO19_88_0_,
        pessoa1_1_.NUMERO_TITULO_ELEITOR as NUMERO20_88_0_,
        pessoa1_1_.ORGAO_EXPEDIDOR_IDENTIDADE as ORGAO21_88_0_,
        pessoa1_1_.PAIS_NACIONALIDADE_ID as PAIS27_88_0_,
        pessoa1_1_.PROCURADOR as PROCURADOR88_0_,
        pessoa1_1_.SERIE_CTPS as SERIE23_88_0_,
        pessoa1_1_.SEXO as SEXO88_0_,
        pessoa1_1_.UF_CTPS_ID as UF28_88_0_,
        pessoa1_1_.UF_IDENTIDADE_ID as UF29_88_0_,
        pessoa1_1_.UF_OAB_MADEP_ID as UF30_88_0_,
        pessoa1_2_.NOME_FANTASIA as NOME1_120_0_,
        pessoa1_2_.NOME_FANTASIA_FONETICO as NOME2_120_0_,
        pessoa1_2_.NUMERO_CNPJ as NUMERO3_120_0_,
        pessoa1_2_.RAZAO_SOCIAL as RAZAO4_120_0_,
        pessoa1_2_.RAZAO_SOCIAL_FONETICO as RAZAO5_120_0_,
        pessoa1_3_.COD_TIPO_AUTORIDADE_COATORA as COD1_172_0_,
        case 
            when pessoa1_1_.id is not null then 1 
            when pessoa1_2_.id is not null then 2 
            when pessoa1_3_.id is not null then 3 
            when pessoa1_.id is not null then 0 
        end as clazz_0_,
        uf4_.id as id147_1_,
        uf4_.version as version147_1_,
        uf4_.COD_CNJ as COD3_147_1_,
        uf4_.COD_IBGE as COD4_147_1_,
        uf4_.COD_STF as COD5_147_1_,
        uf4_.NOME as NOME147_1_,
        uf4_.PAIS_ID as PAIS8_147_1_,
        uf4_.SIGLA as SIGLA147_1_,
        pais5_.id as id173_2_,
        pais5_.version as version173_2_,
        pais5_.COD_IBGE as COD3_173_2_,
        pais5_.COD_TRIBUNAL as COD4_173_2_,
        pais5_.NOME as NOME173_2_ 
    from
        USUARIO this_ 
    left outer join
        PESSOA pessoa1_ 
            on this_.PESSOA_ID=pessoa1_.id 
    left outer join
        PESSOA_FISICA pessoa1_1_ 
            on pessoa1_.id=pessoa1_1_.id 
    left outer join
        PESSOA_JURIDICA pessoa1_2_ 
            on pessoa1_.id=pessoa1_2_.id 
    left outer join
        PESSOA_AUTORIDADE_COATORA pessoa1_3_ 
            on pessoa1_.id=pessoa1_3_.id 
    left outer join
        UF uf4_ 
            on pessoa1_1_.UF_OAB_MADEP_ID=uf4_.id 
    left outer join
        PAIS pais5_ 
            on uf4_.PAIS_ID=pais5_.id 
    where
        this_.id=?		