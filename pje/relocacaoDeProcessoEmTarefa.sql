﻿/*Atualizar o id_jbpm para o mesmo valor de procinst_ da tabela  "public"."jbpm_taskinstance"*/
 
update "core"."tb_processo"
set "id_jbpm" = 4468531
where "id_processo" = 10853;
 
/*Atualizar os valores valores abaixo da tabela "public"."jbpm_taskinstance", isto é para tornar linha com id=4468533
a tarefa atual e terminar a tarefa de linha com id 4584321*/
 
update JBPM_TASKINSTANCE
set
    end_ = NULL,
    isopen_ = true,
    issignalling_ = true
where
    id_ = 4468533;
 
update JBPM_TASKINSTANCE
set
    start_ = now(),
    end_ = now(),
    isopen_ = false,
    issignalling_ = false
where
    id_ = 4584321;
 
 
/*Inserir uma linha na tabela "core"."tb_proc_localizacao_ibpm" onde o valor de "id_processinstance_jbpm" é o mesmo de procinst_ da linha da tarefa atual, ou seja, a linha de id=4468533 da tabela "public"."jbpm_taskinstance"*/
 
insert into "core"."tb_proc_localizacao_ibpm" ("id_task_jbpm"
        ,"id_processinstance_jbpm"
        ,"id_processo"
        ,"id_localizacao"
        ,"id_papel")
values (4449242
        ,4468531
        ,10853
        ,145
        ,1338);